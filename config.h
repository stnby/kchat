#ifndef CONFIG_H
#define CONFIG_H

#define PORT 1337
#define BUF_IN_SIZE 512
#define MAX_CLIENTS 10
#define MOTD  "\r\e[1;34m" \
"             __\n" \
"          -=(o '.\n" \
" \e[3mKernal\e[1m      '.-.\\\n" \
" \e[3mCommunity\e[1m   /|  \\\\\n" \
" \e[3m2017-2020\e[1m   '|  ||\n" \
"             _\\_):,_" \
"\e[0m\n"

#endif